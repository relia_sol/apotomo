Rails.application.routes.draw do
  match "application/render_event_response",
        controller: "application", action: "render_event_response",
        to: "application/render_event_response", via:  [:get, :post, :put, :patch, :delete]
end
